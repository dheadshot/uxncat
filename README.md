# uxn cat

A simple "cat" program (takes input and returns as output) for uxn, with a visual display in pixels for confirmation (it draws lines on the graphical output with one pixel for each character input).  It exits when it encounters a lowercase letter `q`.  Used for testing console I/O on uxn emulators.

## Compiling

uxn cat con be compiled using `uxnasm` or inside uxn using `drifblim`.  For `uxnasm`, run `uxnasm cat.tal cat.rom`.

A prebuilt binary is included in this [upload](https://codeberg.org/dheadshot/uxncat/src/branch/main/cat.rom).

## Running

Open with a uxn emulator such as [uxnemu](https://git.sr.ht/~rabbits/uxn/) or [uxn11](https://git.sr.ht/~rabbits/uxn11/).

## Licence

MIT License

Copyright (c) 2023 DHeadshot's Software Creations

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.